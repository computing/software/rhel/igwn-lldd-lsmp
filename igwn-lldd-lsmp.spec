# -- metadata

%define srcname igwn-lldd-lsmp
%define release 1.1

Name:      python-%{srcname}
Version:   0.2.0
Release:   %{release}%{?dist}
Summary:   Kafka LSMP clients for delivery of low-latency h(t) data

License:   GPL
Url:       https://git.ligo.org/computing/lowlatency/igwn-lldd-lsmp
Source0:   https://software.igwn.org/lscsoft/source/igwn-lldd-lsmp-%{version}.tar.gz
Packager:  LVC Computing <lvccomputing@ligo.org>
Vendor:    LVC Computing <lvccomputing@ligo.org>
BuildArch: noarch

#
# Let this conflict with the old python2 version
# Conflicts: python2-llkafka

# rpmbuild dependencies
BuildRequires: python-srpm-macros
BuildRequires: python-rpm-macros
BuildRequires: python3-rpm-macros

# build dependencies
BuildRequires: python%{python3_pkgversion}-pip
BuildRequires: python%{python3_pkgversion}-setuptools >= 38.2.5
BuildRequires: python%{python3_pkgversion}-wheel

%description
The IGWN - Low Latency Data Distribution (lldd) is software to
distribute low latency data used by the International
Gravitational-Wave Observatory Network (IGWN).

# -- packages

%package -n python%{python3_pkgversion}-%{srcname}
Summary:  Python %{python3_version} library for IGWN LLDD client
Requires: python%{python3_pkgversion}-igwn-lldd-common >= 0.4
Requires: gds-pygds
Requires: python%{python3_pkgversion}-configargparse
# https://python-rpm-porting.readthedocs.io/en/latest/tools.html#python-provide
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
%description -n python%{python3_pkgversion}-%{srcname}
The IGWN - Low Latency Data Distribution (lldd) is software to
distribute low latency data used by the International
Gravitational-Wave Observatory Network (IGWN).
This package provides the Python %{python3_version} libraries.

%package -n %{srcname}
Summary: Command line utilities for igwn-lldd-lsmp
Requires: python%{python3_pkgversion}-%{srcname} = %{version}-%{release}
# This is basically a name change from the old package (python2-llkafka) to this package
# We could instead use Conflicts: python2-llkafka, but then packages which depend on
# python2-llkafka would complain.
Obsoletes: python2-llkafka < 0.2.1-1.1
Provides: python2-llkafka = 0.2.1-1.1
%description -n %{srcname}
The IGWN - Low Latency Data Distribution (lldd) is software to
distribute low latency data used by the International
Gravitational-Wave Observatory Network (IGWN).
This package provides the command-line interfaces to the LSMP
shared memory.

# -- build steps

%prep
%autosetup -n %{srcname}-%{version}

%build
%py3_build_wheel

%install
%py3_install_wheel igwn_lldd_lsmp-%{version}-*.whl

%clean
rm -rf $RPM_BUILD_ROOT

# -- files

%files -n %{srcname}
%license LICENSE
%doc README.md
%{_bindir}/*

%files -n python%{python3_pkgversion}-%{srcname}
%license LICENSE
%doc README.md
%{python3_sitelib}/*

# -- changelog

%changelog
* Tue Oct 11 2022 Patrick Godwin <patrick.godwin@ligo.org> - 0.2.0-1
- 0.2.0 release

* Tue Oct 11 2022 Adam Mercer <adam.mercer@ligo.org> - 0.1.0-1.2
- add %{?dist} to release

* Mon Oct 10 2022 Adam Mercer <adam.mercer@ligo.org> - 0.1.0-1.1
- remove duplicate source line

* Sun Sep 18 2022 LVC Computing <lvccomputing@ligo.org> - 0.1.0-1
- Initial RPM packaging
